import { Link } from 'gatsby'
import React from 'react'
import styled from 'styled-components'

import NavbarButtons from '~src/components/molecules/NavbarButtons'
import { styledTheme } from '~src/constants/theme'
import FireIcon from '~src/images/fireIcon.svg'

const Container = styled.header`
  align-items: center;
  background-color: ${styledTheme.colors.crimsonRed};
  background-size: -25%;
  display: flex;
  justify-content: space-between;
  margin-bottom: 24px;
  padding: 12px 10%;

  h1 {
    color: white;
    display: inline-block;
    margin: 0;
  }
`

const Logo = styled(FireIcon)`
  height: 2.25rem;
  margin-right: 12px;
  width: 2.25rem;
`

type Props = {
  siteTitle: string
}

const Navbar = ({ siteTitle }: Props): React.ReactElement => (
  <Container>
    <Link to="/" >
      <Logo />
      <h1>{siteTitle}</h1>
    </Link>
    <NavbarButtons />
  </Container>

)

export default Navbar
