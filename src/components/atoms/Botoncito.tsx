import styled, { css } from 'styled-components'

import { styledTheme } from '~src/constants/theme'

const { colors } = styledTheme

type Props = {
  minimal?: boolean
}

const Botoncito = styled.a<Props>`  
  background: ${colors.logoBlack};
  border-radius: 3px;
  color: white;
  cursor: pointer;
  display: inline-block;
  padding: 16px;
  user-select: none;

  /* If <Button> has a "minimal" prop
   * it will have these rules! */
  ${props => props.minimal && css`
    background: transparent;
    border: 2px solid ${colors.logoBlack};
    color: ${colors.logoBlack};
  `}
`

export default Botoncito
