import React, { MutableRefObject, useEffect, useRef } from 'react'
import styled from 'styled-components'

import { styledTheme } from '~src/constants/theme'

const StyledInput = styled.input`
  background: ${styledTheme.colors.white};
  border: 1px solid ${styledTheme.colors.iron};
  border-radius: 2px;

  color: #3C4858;
  cursor: pointer;
  display: flex;
  font-family: "Open Sans", sans-serif;
  font-weight: 600;
  justify-content: center;
  padding: 9px 18px;
`

type Props = {
  autoFocus?: boolean,
  className?: string,
  type: string,
  onChange: (value: string) => void
  onKeyEnter: () => void
}

const Input = (props: Props) => {
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => props.onChange(event.target.value)
  const handleKeyDown = (event: React.KeyboardEvent) => { if (event.key === 'Enter') props.onKeyEnter() }
  const inputRef = useRef<HTMLInputElement>(null) as MutableRefObject<HTMLInputElement>

  if (props.autoFocus) {
    useEffect(() => {
      inputRef.current.focus()
    }, [])
  }

  return <StyledInput
    className={props.className}
    type={props.type}
    ref={inputRef}
    onChange={handleChange}
    onKeyDown={handleKeyDown}
  />
}

export default Input
