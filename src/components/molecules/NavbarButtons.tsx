import { navigate } from 'gatsby'
import React from 'react'
import styled from 'styled-components'

import Botoncito from '~src/components/atoms/Botoncito'
import { styledTheme } from '~src/constants/theme'

const Container = styled.div`
    ${Botoncito} {
        background-color: ${styledTheme.colors.crimsonRed};
        padding: 24px;
        transition: all 150ms ease-in-out;

        &:hover {
            background-color: red;
            padding: 24px 36px;
        }
    }
`

const NavbarButtons = () => (
  <Container>
    <Botoncito onClick={() => navigate('/')}>Index</Botoncito>
    <Botoncito onClick={() => navigate('/404')} >404</Botoncito>
  </Container>
)

export default NavbarButtons
