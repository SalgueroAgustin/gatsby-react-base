import React from 'react'
import styled from 'styled-components'

import Botoncito from '~src/components/atoms/Botoncito'
import { styledTheme } from '~src/constants/theme'

const Tooltip = styled.div`
    background-color: ${styledTheme.colors.charcoalText};
    border-radius: 5px;
    color: white;
    margin: 10px 15px;
    padding: 5px;
    position: relative;
    text-align: center;
    width: 200px;

    &::before {
        background-color: ${styledTheme.colors.charcoalText};
        content: ' ';
        height: 20px;
        left: 8%;
        position: absolute;
        top: 50%;
        transform: translate(calc(-100% - 5px), -50%) rotate(45deg);
        width: 20px;
    }
`

const Container = styled.div`
    display: flex;
    margin: 30px 0;
    width: fit-content;

    ${Tooltip} {
        display: none;
    }

    &:hover {
        ${Tooltip} {
            display: block;
        }
    }
`

const BotonConTooltip = () => {
  return <Container>
    <Botoncito>Yo tengo una tooltip</Botoncito>
    <Tooltip>Soy la tooltip</Tooltip>
  </Container>
}

export default BotonConTooltip
