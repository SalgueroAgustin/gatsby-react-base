import 'normalize.css'

import { graphql, useStaticQuery } from 'gatsby'
import React from 'react'
import Helmet from 'react-helmet'
import styled from 'styled-components'

import GlobalStyle from '~src/components/GlobalStyle'
import Navbar from '~src/components/organisms/Navbar'
import { styledTheme } from '~src/constants/theme'
import favicon from '~src/images/favicon.ico'

const Container = styled.div`
  margin: 0 auto;
  max-width: 960px;
`

const Main = styled.main`
  margin-bottom: 24px;
`

const Footer = styled.footer`
  border-top: 1px solid ${styledTheme.colors.lightCharcoal};
  color: ${styledTheme.colors.mediumCharcoal};
  font-size: 0.8rem;
  padding: 24px 0;
`

type Props = {
  children: React.ReactNode
}

const Layout = ({ children }: Props): React.ReactElement => {
  const pageData = useStaticQuery(graphql`
    query PageDataQuery {
      site {
        siteMetadata {
          title
          description
          author
        }
      }
    }
  `)
  const siteMetadata = pageData.site.siteMetadata
  return (
    <>
      <GlobalStyle />
      <Helmet>
        <link rel="icon" href={favicon} />
      </Helmet>
      <Navbar siteTitle={siteMetadata.title} />
      <Main>
        <Container>
          {children}
        </Container>
      </Main>
      <Footer>
        <Container>
          &copy; {new Date().getFullYear()} - {siteMetadata.author}
        </Container>
      </Footer>
    </>
  )
}

export default Layout
