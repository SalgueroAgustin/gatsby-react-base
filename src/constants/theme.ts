export const styledTheme = {
  // Brand Colors
  colors: {
    // background
    blush: '#F2C1E4',
    coconut: '#FFE8E0',
    sky: '#A8E2F7',
    crimsonRed: '#C00',

    // functional
    adroll: '#00AEEF',
    eggplant: '#8E44AD',
    eggplantDark: '#BF4088',
    indigo: '#030303',

    // accent
    basil: '#4C805A',
    carrot: '#F87060',
    lemon: '#FDCD0A',
    taffy: '#CC66A0',

    // grayscale
    charcoalText: '#3C4858',
    extraLightCharcoal: '#F4F6F9',
    iron: '#DADCE0',
    lightCharcoal: '#E5E9F2',
    logoBlack: '#333333',
    mediumCharcoal: '#8492A6',
    white: '#FFFFFF',
  },
}
