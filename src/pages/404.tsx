import React from 'react'

import Seo from '~src/components/atoms/Seo'
import PageBase from '~src/components/PageBase'

const IndexPage = (): React.ReactElement => (
  <PageBase>
    <Seo title="Oops" />
    <h1>Error 404</h1>
  </PageBase>
)

export default IndexPage
