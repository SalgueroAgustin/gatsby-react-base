
import React, { useState } from 'react'

import Botoncito from '~src/components/atoms/Botoncito'
import Seo from '~src/components/atoms/Seo'
import BotonAlerta from '~src/components/molecules/BotonAlerta'
import BotonConTooltip from '~src/components/molecules/BotonConTooltip'
import PageBase from '~src/components/PageBase'

const IndexPage = () => {
  const [encendido, cambiarEncendido] = useState(false)

  function alternarEncendido () {
    cambiarEncendido(!encendido)
  }

  return (
    <PageBase>
      <Seo title="Home"/>
      <Botoncito minimal={encendido} onClick={alternarEncendido}>
        Soy un botón que se prende y se apaga
      </Botoncito>
      <BotonConTooltip />
      <BotonAlerta />
    </PageBase>
  )
}

export default IndexPage
