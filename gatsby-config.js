const path = require('path')

module.exports = {
  pathPrefix: '/gatsby-react-base',
  siteMetadata: {
    title: 'AGus',
    description: 'Descripción de tu app',
    author: 'Tu nombre'
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'images',
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: 'gatsby-plugin-eslint',
      options: {
        test: /\.js$|\.jsx$/,
        exclude: /(node_modules|.cache|public)/,
        options: {
          emitWarning: true,
          failOnError: false,
        },
      },
    },
    'gatsby-transformer-sharp',
    'gatsby-plugin-sharp',
    {
      resolve: 'gatsby-plugin-alias-imports',
      options: {
        alias: {
          '~src': path.join(__dirname, 'src'),
        },
        extensions: ['jsx', 'js'],
      },
    },
    'gatsby-plugin-offline',
    'gatsby-plugin-remove-console',
    {
      resolve: 'gatsby-plugin-web-font-loader',
      options: {
        google: {
          families: ['Open Sans:400,600,700', 'Work Sans:600,700'],
        },
        display: 'swap',
      },
    },
    'gatsby-plugin-styled-components',
    'gatsby-plugin-loadable-components-ssr',
    {
      resolve: 'gatsby-plugin-react-svg',
      options: {
        rule: {
          include: /images\/.*\.svg/,
        },
      },
    },
    {
      resolve: 'gatsby-plugin-typescript',
      options: {
        isTSX: true, // defaults to false
        allExtensions: true, // defaults to false
      },
    },
  ],
}
